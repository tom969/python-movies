"""This modules is for representing movies"""
class Movie(object):
    """This class representd a movie"""

    def __init__(self, genre, length):
        self.genre = genre
        self.length = length
        